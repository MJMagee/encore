module.exports = function(grunt) {

  var app = 'app',
  appAssets = 'assets',
  publicAssets = 'public-assets';

  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
  grunt.initConfig({


    // Set-up a local server
      connect: {
        server: {
          options: {
            port: 8888,
            hostname: '*',
            keepalive: true,
            middleware: function(connect, options, middlewares) {
              var modRewrite = require('connect-modrewrite');
              middlewares.unshift(modRewrite(['!\\.html|\\.js|\\.svg|\\.css|\\.jpg$|\\.gif$|\\.png$ /index.html [L]']));
              return middlewares;
            }
          }
        }
      },


    // Change SCSS files to CSS files
      sass: {
        build: {
          files: [{
            expand: true,
            cwd: appAssets,
            src: ['css/**/*.scss'],
            dest: publicAssets,
            ext: '.css'
          }]
        }
      },


    // Add vendor prefixes
       postcss: {
          options: {
            map: {
              inline: false,
              annotation: publicAssets + '/css'
            },
            processors: [
              require('autoprefixer')({browsers: 'last 3 versions'}) // add vendor prefixes
            ]
          },
          dist: {
            expand: true,
            cwd: publicAssets,
            src: ['css/**/*.css'],
            dest: publicAssets
          }
        },


    // Handle JS
      shell: {
         command: 'webpack --display-error-details'
      },


    // Copy across files
      copy: {

        images: {
          files: [{
            expand: true,
            cwd: appAssets,
            src: ['img/**/*'],
            dest: publicAssets,
            timestamp: true
          }]
        }
      },


    // Notify
      notify: {
        complete: {
          options: {
            title: 'Done!',
            message: 'Grunt has built ALL THE THINGS!'
          }
        }
      },


    // Set up watchers for development - webpack has its own watcher for js
      watch: {
        css: {
          files: [appAssets + '/css/**/*.scss'],
          tasks: ['sass', 'postcss', 'notify:complete']
        },
        js: {
          files: [app + '/**/*', 'webpack.config.js'],
          tasks: ['shell']
        }
      }
  });


  grunt.registerTask('build', [
    'shell',
    'sass',
    'postcss',
    'newer:copy',
    'notify:complete',
    'connect'
  ]);

};
