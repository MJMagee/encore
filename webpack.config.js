var webpack = require('webpack'),
path = require('path'),

appPath = path.resolve(__dirname, 'app'),
buildPath = path.resolve(__dirname, 'public-assets/js'),

config = {
  entry: appPath + '/app.jsx',
  output: {
    path: buildPath,
    filename: 'core.js'
  },


  resolve: {
    modulesDirectories: [ 'node_modules', 'app' ],
    extensions: [ '', '.js', '.jsx' ]
  },
  module: {
    loaders: [
      {
        test: /\.json$/,
        loader: 'json-loader'
      },

      {
        test: /\.jsx?$/,
        loader: "babel",
        exclude: /node_modules/,
        query: {
          presets: ["react", "es2015"],
          cacheDirectory: true
        }
      },

      {
        test: require.resolve('react'),
        loader: 'expose?React'
      }
    ]
  }
};

module.exports = config;