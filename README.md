# Technical Challenge for Encore

1. Run: `npm install`
2. Run: `grunt build`
3. Navigate to [localhost on port 8888](http://localhost:8888)
4. Think, Wow, what a smashing dev this Mell person is.
5. Hire Mell.


## Things which could be improved

- There aren't any automated tests!
- The stop icon doesn't revert once the preview has ended.
- The fixed LI heights aren't going to play nicely with big titles.
- It would be better to have a pause button instead of stop (stopping only when a new track is started without the user manually ending the currently playing track).
- There should be functionality to look up IDs rather than hard-coding them in.
- It would be better to cache the data and images to the server rather than calling the API every time.
- The design could be a lot less functional looking and do a lot more on desktop when it has the space, especially with the big images we're getting.