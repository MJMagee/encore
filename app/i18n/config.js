// The text is bundled together from each different module into text

// TO DO: this needs to actually work out which language it should return
// But for now, it can just return English

import text from 'i18n/text/en-gb/en-gb'

export default {
    i18n: text
};