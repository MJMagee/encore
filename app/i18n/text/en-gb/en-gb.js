// Export all of the modules as a single object to make interacting with any language easier

import _ from 'underscore';

import common from 'i18n/text/en-gb/common/common';
import layout from 'i18n/text/en-gb/common/layout';
import artists from 'i18n/text/en-gb/artists/artists';
import errors from 'i18n/text/en-gb/artists/errors';


var text = _.extend({},
  common,
  layout,
  artists,
  errors
);


export default text;
