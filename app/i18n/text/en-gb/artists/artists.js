export default {


  // Artists
      'Most popular tracks': 'Most popular tracks',
      'Most popular tracks, according to Spotify': '[$1] top [$2] tracks, according to Spotify.',
      'See the top 10': 'See [$1] top ten tracks',

      'Related artists': 'Related artists'
};