export default {


  // Errors
      'Uh-oh. Something\'s gone wrong here': 'Uh-oh. Something\'s gone wrong here.',
      'We can\'t find the artist you\'re looking for': 'Sorry, mate. We can\'t find the artist you\'re looking for. And we even looked behind the couch cushions.'
};