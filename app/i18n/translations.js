import config from 'i18n/config';

export default {

    //  If a translation is available, use it, otherwise default to the string provided
      putOut: function(s) {
          if (typeof s!='undefined' && typeof config.i18n!='undefined' && config.i18n[s]) {
            var subs=config.i18n[s];

            // Strong and span tags
              subs=subs.replace(/\[strong\]([^/]+)\[\/strong\]/g, '<strong>$1</strong>');
              subs=subs.replace(/\[span\]([^/]+)\[\/span\]/g, '<span>$1</span>');

            // Links
              subs=subs.replace(/\[link\="([^"]*)"([^\]]*)\](.*?)\[\/link\]/g, '<a href="$1"$2>$3</a>');

            // Take any additional arguments we may have just set up and substitute them
            // This will find [$1], [$2], etc., and replace them with additional arguments sent to putOut()
            // in the order they are passed in
              for (var i=1; i<arguments.length; i++) {
                subs=subs.replace(new RegExp('\\[\\$'+i+'\\]', 'g'), arguments[i]);
              }

            return subs;
          }

          return s;
      },



    // Add an apostrophe or apostrophe and an S to a word
      possessiveS: function(word) {
        var lastLetter=word.slice(-1);

        if (lastLetter=='\'') { return word; }
        else if (lastLetter=='s') { return word+'\''; }
        else { return word+'\'s'; }
      }

  };
