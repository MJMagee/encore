import reactor from 'reactor';
import actionTypes from './action-types';
import { browserHistory } from 'react-router';
import { Map } from 'immutable';
import axios from 'axios';

import translations from 'i18n/translations';

import DefaultArtists from 'components/artists/DefaultArtists';

export default {

  // Fetch the basic data for all artists
    fetchAllInfo: function() {
      DefaultArtists.map((id, key) => {
        axios.get(`https://api.spotify.com/v1/artists/${id}`)
          .then((response) => {
            reactor.dispatch(actionTypes.RECEIVED_INFO, {
              key: key,
              data: response
            });
        });
      });
    },


  // Fetch track data for an artist by their ID
    fetchTracks: function(key) {
      var id = reactor.actions.artists.getData(key);

      // If we have the id of the endpoint we need to call, call it
        if (id) {

          axios.get(`https://api.spotify.com/v1/artists/${id}/top-tracks?country=GB`)
            .then((response) => {
              reactor.dispatch(actionTypes.RECEIVED_TRACKS, {
                key: key,
                data: response
              });

              // If the page url doesn't include the key, change the browser history
              // At this point, the data is saved in the store and changing the url
              // won't trigger a second, unneeded fetch
                if (location.pathname.indexOf(key)<0) { browserHistory.replace(`/artists/${key}`); }

          });
        }

      // Otherwise, show an error message
        else {
          reactor.dispatch(actionTypes.NOT_FOUND, { key: key });
        }

    },


  // Set if a track is playing or not
    trackIsPlaying: function(key, track, trackIsPlaying) {
        reactor.dispatch(actionTypes.TRACK_IS_PLAYING, {
          key: key,
          track: track,
          trackIsPlaying: trackIsPlaying
        })
    },


  // Get an artist's data
    getData: function(key) {
      let data = DefaultArtists.get(key);

      if (data) { return data; }
      else {

        // If the key isn't set in DefaultArtists, we could call the Spotify API
        // to query for the artist's ID and show the data if it exists,
        // or show an error if we can't find them
        // For now, we're not doing either of those because why would
        // anyone want to listen to a song by anyone other than Idina Menzel
        // or Tim Minchin?! That's madness!

        return false;
      }
    },


  // Update an artist's top x track limit
    setTrackLimit: function(key, limit) {
      reactor.dispatch(actionTypes.LIMIT_TRACKS, {
        key: key,
        limit: limit
      });
    }

};
