import { Store, toImmutable } from 'nuclear-js';
import { List, Map, Set, OrderedMap } from 'immutable';
import actionTypes from './action-types';

import TrackLimits from 'components/songs/TrackLimits';

export default Store({
  getInitialState() {
    return toImmutable({
      artists: new Map()
    });
  },

  initialize() {
    this.on(actionTypes.RECEIVED_INFO, setInfo);
    this.on(actionTypes.RECEIVED_TRACKS, setTrackData);
    this.on(actionTypes.LIMIT_TRACKS, limitTracks);
    this.on(actionTypes.TRACK_IS_PLAYING, trackIsPlaying);
    this.on(actionTypes.NOT_FOUND, notFound);
  }
});


// Set an artist's data
  function setInfo(state, { key, data }) {
    data = toImmutable(data);
    let images = data.getIn(['data', 'images']);
    let artistImages = {};

    if (images) {
      images.takeLast(2).map(data => {
        if (data.get('width') < 200) { artistImages['thumb'] = data.get('url'); }
        else { artistImages['large'] = data.get('url'); }
      })
    }

    return state
      .setIn(['artists', key, 'name'], data.getIn(['data', 'name']))
      .setIn(['artists', key, 'images'], (images ? toImmutable(artistImages) : false));
  }


// Set an artist's track information
  function setTrackData(state, { key, data }) {
    data = toImmutable(data);

    let tracks = data.getIn(['data', 'tracks'])
    .toMap().mapEntries(([k, v]) => {
      return [
        k,
        v
          .set('short-name', truncate(v.get('name')))
          .setIn(['album', 'short-name'], truncate(v.getIn(['album', 'name'])))
      ];
    });

    return state
      .setIn(['artists', key, 'tracks'], tracks)
      .setIn(['artists', key, 'showing'], TrackLimits.get('defaultLimit'));
  }


// Change how many tracks are showing
  function limitTracks(state, { key, limit }) {
    return state.setIn(['artists', key, 'showing'], limit);
  }


// Set whether a song is playing or not
  function trackIsPlaying(state, { key, track, trackIsPlaying }) {

    // Loop through all of the artist's tracks, setting the playing
    // value to true if trackIsPlaying is true and we're looking at the track
    // we need to play, otherwise setting it to false
    // This will stop any playing tracks if a new one is clicked
      let tracks = toImmutable(state.getIn(['artists', key, 'tracks']))
      .toMap().mapEntries(([k, v]) => {
        return [ k, v.set('playing', (trackIsPlaying ? k==track : false)) ];
      });

    return state.setIn(['artists', key, 'tracks'], tracks);
  }


// Update data to show error
  function notFound(state, { key }) {
    return state.setIn(['artists', key, 'error'], true);
  }


// Truncate words
  function truncate(str) {
    var words=str.split(' '),
    truncatedStr='';

    if (words.length && words.length>6) {
      for (var i=0; i<=5; i++) { truncatedStr+=words[i]+(i==5 ? '…' : ' '); }
    }
    else { truncatedStr=str; }

    return truncatedStr;
  }