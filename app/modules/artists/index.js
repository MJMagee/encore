import actions from './actions';
import actionTypes from './action-types';
import getters from './getters';
import store from './store';

export default function(reactor) {
  reactor.registerStores({
    artists: store
  });
  reactor.getters.artists = getters;
  reactor.actionTypes.artists = actionTypes;
  reactor.actions.artists = actions;
}
