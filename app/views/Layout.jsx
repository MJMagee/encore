import React from 'react';
import reactor from 'reactor';
import translations from 'i18n/translations';

const Layout = React.createClass({
  render() {
    return (

      <div id="wrapper-container">
        <div id="wrapper">
          { this.props.children }
        </div>

        <div id="footer">
          <p dangerouslySetInnerHTML={{ __html: translations.putOut('Coded by M. J. Magee') }} />
        </div>

      </div>
    );
  }
});

export default Layout;