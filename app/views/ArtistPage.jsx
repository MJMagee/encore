import React from 'react';
import reactor from 'reactor';
import { browserHistory, Link } from 'react-router';
import smoothScroll from 'smoothscroll';

import FetchDataMixin from 'mixins/FetchData';
import translations from 'i18n/translations';

import DefaultArtists from 'components/artists/DefaultArtists';
import TrackLimits from 'components/songs/TrackLimits';
import LoadingIndicator from 'components/common/LoadingIndicator';
import ContentWrapper from 'components/common/ContentWrapper';
import TrackListing from 'components/songs/TrackListing';

const ArtistPage = React.createClass({
  mixins: [ reactor.ReactMixin, FetchDataMixin ],
  observeParams: ['artist'],


  // Get data
    getDataBindings() {
      return {
        artists: reactor.getters.artists.artists
      };
    },


    componentDidMount() {
      reactor.actions.artists.fetchAllInfo();
    },


    fetchData(props, state) {
      if (!this.tracks(state, props)) {
        reactor.actions.artists.fetchTracks(this.getArtist(props));
      }
    },


    // Allow for props to be passed in, in case the props have newly been passed into fetchData
      getArtist(props = this.props) {
        let artist = props.params.artist;
        return artist ? artist : DefaultArtists.keySeq().first(); // Set a default artist if no key is set
      },

      artist(state, props = this.props) {
        return state.artists.get(this.getArtist(props));
      },

      tracks(state, props = this.props) {
        return state.artists.getIn([this.getArtist(props), 'tracks']);
      },



  // Handle the top track limit
    defaultLimit: TrackLimits.get('defaultLimit'),
    maxLimit: TrackLimits.get('maxLimit'),

    setLimit(e, key = this.getArtist(), limit = this.defaultLimit) {
      e.preventDefault();
      e.stopPropagation();

      reactor.actions.artists.setTrackLimit(key, limit);
    },



  // Change artist view
    changeArtist(e, oldArtist, newArtist) {
      e.preventDefault();
      e.stopPropagation();

      reactor.actions.artists.setTrackLimit(oldArtist, this.defaultLimit);
      browserHistory.push(`/artists/${newArtist}`);
      smoothScroll(document.getElementById('container'));
    },



  // Handle playing and pausing tracks
    trackIsPlaying(key, track, trackIsPlaying) {
      reactor.actions.artists.trackIsPlaying(key, track, trackIsPlaying);
    },



  // Render the view
    render() {

      // If we have the data, show it
          let artist = this.artist(this.state);
          if (artist) {

            // If there's an error, show it
              if (artist.get('error')) {
                return (
                  <ContentWrapper {...this.props}>
                    <div className="message error">
                      <p className="title">{ translations.putOut('Uh-oh. Something\'s gone wrong here') }</p>
                      <p>{ translations.putOut('We can\'t find the artist you\'re looking for') }</p>
                    </div>
                  </ContentWrapper>
                );
              }


            // Otherwise, show the track listings
              else {

                // As the info and tracks are loaded in two separate requests
                // make sure we have them both
                  if (artist.get('name') && artist.get('tracks')) {
                    let artistKey = this.getArtist();
                    let limit = artist.get('showing');

                    return (
                      <ContentWrapper {...this.props} title={ artist.get('name') }>

                        <p className="img">
                          <img src={
                            artist.get('images') && artist.getIn(['images', 'large']) ?
                              artist.getIn(['images', 'large']) :
                              '/public-assets/img/defaults/large.jpg'
                          } />
                        </p>

                        <div id="main-content">
                          <h2>{ translations.putOut('Most popular tracks') }</h2>
                          <p className="subtitle">
                            {
                              translations.putOut('Most popular tracks, according to Spotify',
                              translations.possessiveS(artist.get('name')),
                              limit)
                            }
                          </p>

                          <ol id="tracklist">
                            {
                              // Loop through the tracks array to put out as many
                              // tracks as the limit allows
                              // NB: we're trusting that the tracks are already in order.
                              // Spotify itself seems to do this, as Idina Menzel's
                              // 'Small World' at 42 comes inbetween 'First Time in Forever'
                              // at 49 and 'For Good' at 48 on Spotify's site.
                              // It would be trivial to loop through the data in the store and
                              // assign a different order if we wanted to, but it seemed to
                              // fit the brief better to keep Spotify's order.

                              artist.get('tracks').entrySeq().take(limit).map(([key, data]) => {
                                return <TrackListing
                                  key={ key }
                                  artist={ artistKey }
                                  track={ key }
                                  data={ data }
                                  trackIsPlaying={ this.trackIsPlaying }
                                />
                              })

                            }
                          </ol>

                          { limit < this.maxLimit ?
                            <p className="cta">
                              <a href="#" onClick={ (e)=> this.setLimit(e, artistKey, this.maxLimit) }>
                                { translations.putOut('See the top 10', translations.possessiveS(artist.get('name')))
                                }
                              </a>
                            </p> :
                            ''
                          }

                        </div>


                        <div id="sidebar">
                          <h2>{ translations.putOut('Related artists') }</h2>
                          <ul>
                            {
                              DefaultArtists.keySeq().map(key => {
                                if (key == artistKey) { return false; }

                                let relatedArtist = this.state.artists.get(key);
                                if (relatedArtist) {
                                  return <li key={ key } onClick={ (e)=> this.changeArtist(e, artistKey, key) }>

                                    <img src={
                                      relatedArtist.get('images') && relatedArtist.getIn(['images', 'thumb']) ?
                                        relatedArtist.getIn(['images', 'thumb']) :
                                        '/public-assets/img/defaults/thumb.jpg'
                                    } />

                                    <Link to={ `/artists/${key}` }>
                                      { relatedArtist.get('name') }
                                    </Link>
                                  </li>

                                }
                              })
                            }
                          </ul>
                        </div>

                      </ContentWrapper>
                    );

                  }

              }
          }


       // Otherwise, show the spinner
          return (<LoadingIndicator />);
    }
});

export default ArtistPage;