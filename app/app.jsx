import 'babel-polyfill';
import 'scripts/cssua';
import 'scripts/modernizr';

import React from 'react';
import ReactDOM from 'react-dom';
import reactor from 'reactor';
import Router from 'router';
import Immutable from 'immutable';
import installDevTools from 'immutable-devtools';

window.reactor = reactor;
installDevTools(Immutable);


// Attach the router to the container div
  ReactDOM.render(Router, document.getElementById('container'));