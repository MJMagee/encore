// Import all of the files set up in modules
  import { Reactor } from 'nuclear-js';

  const reactor = new Reactor({
    debug: true
  });
  reactor.actionTypes = {};
  reactor.actions = {};
  reactor.getters = {};


  import registerArtists from 'modules/artists/index';
  registerArtists(reactor);


  export default reactor;