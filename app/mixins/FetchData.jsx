// Fetch data when the component loads and when the state is changed
  import _ from 'underscore';

  const FetchDataMixin = {
    componentDidMount: function() {
      this.fetchData(this.props, this.state);
    },

    componentWillUpdate: function(props, state) {
      if (_.some(this.observeState, key => this.state[key] != state[key]) ||
          _.some(this.observeProps, key => this.props[key] != props[key]) ||
          (this.props.params && props.params && _.some(this.observeParams, key => this.props.params[key] != props.params[key]))) {
        this.fetchData(props, state);
      }
    }

  };

  export default FetchDataMixin;
