import React from 'react';
import reactor from 'reactor';

import ReactAudioPlayer from 'react-audio-player';


const TrackListing = React.createClass({

  audioPlayer() {
    return (this.props.data.get('preview_url') && this.player ? this.player.audioEl : false);
  },

  handleAudio() {
    let audioPlayer = this.audioPlayer();

    // If the player is playing, stop it
        if (!audioPlayer.paused && !audioPlayer.ended && audioPlayer.currentTime > 0) { this.props.trackIsPlaying(this.props.artist, this.props.track, false); }

    // Otherwise, start playing
        else { this.props.trackIsPlaying(this.props.artist, this.props.track, true); }

  },


  render() {
    let data = this.props.data;
    let audioPlayer = this.audioPlayer();
    var classes;

    if (audioPlayer) {
      if (data.get('playing')) {
        audioPlayer.play();
        classes = 'playing';
      }
      else {
        audioPlayer.pause();
        audioPlayer.currentTime = 0;
      }
    }

    return (
      <li className={ classes } onClick={ this.handleAudio }>

        { data.getIn(['album', 'images', '1']) ?
          <p className="img"><img src={ data.getIn(['album', 'images', '1', 'url']) } /></p> :
          ''
        }

        <p className="title">
          <span className="track-name" data-short-name={ data.get('short-name') }>
            <span>{ data.get('name') }</span>
          </span>
          { data.getIn(['album', 'name']) ?
            <span className="album" data-short-name={ data.getIn(['album', 'short-name']) }>
              <span>{ data.getIn(['album', 'short-name']) }</span>
            </span> :
            ''
          }
        </p>


        { data.get('preview_url') ?
            <ReactAudioPlayer src={ data.get('preview_url') } ref={(r) => { this.player = r } } />
            : ''
        }

      </li>
    );
  }

});


export default TrackListing;