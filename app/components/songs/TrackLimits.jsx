import { Map } from 'immutable';

export default new Map({
  defaultLimit: 5,
  maxLimit: 10
});