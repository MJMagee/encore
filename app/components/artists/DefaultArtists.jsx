import { Map } from 'immutable';

export default new Map({
  'idina-menzel': '73Np75Wv2tju61Eo9Zw4IR',
  'kristin-chenoweth': '3DgcBA7P0ji5co7Z1Gfp2Q',
  'tim-minchin': '4adgHnoK84DgtLot2jxrp2'
});