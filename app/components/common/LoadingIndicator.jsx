import React from 'react';
import translations from "i18n/translations";

export const LoadingIndicator = React.createClass({
  render() {
    return <p className="loading">{ translations.putOut('Loading') }</p>
  }
});

export default LoadingIndicator;
