import React from 'react';
import translations from 'i18n/translations';

export const ContentWrapper = React.createClass({

  render() {

    return (
      <div id="content" className="clearfix">
        { this.props.title ? <h1>{ translations.putOut(this.props.title) }</h1> : '' }
        { this.props.children }
      </div>
    );
  }
});

export default ContentWrapper;