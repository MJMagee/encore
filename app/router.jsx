import React from 'react';
import reactor from 'reactor';
import { Router, Route, IndexRoute, Redirect, browserHistory } from 'react-router';


import Layout from 'views/Layout';
import ArtistPage from 'views/ArtistPage';



  let router = (
    <Router history={ browserHistory }>

      <Redirect from="/" to="/artists" />

      <Route path="/" component={ Layout }>

        <Route path="artists">
          <IndexRoute component={ ArtistPage } />
          <Route path=":artist" component={ ArtistPage } />
        </Route>

      </Route>
    </Router>
  );

  export default router;